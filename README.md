litter
======

Description
-----------

Wrapper that runs subsequent command with low priority.

Principles
----------

`litter` adjusts:

* niceness;
* scheduler policy and priority;
* I/O class and priority;
* OOM score.

The pre-defined priority sets are:

| Name   | Short CLI switch | Scheduler policy | Scheduler priority | Niceness | I/O class | I/O priority | OOM score | Notes   |
|--------|------------------|------------------|--------------------|----------|-----------|--------------|-----------|---------|
| deity  | -A               | SCHED\_RR        | 99                 | -19      | RT        | 0            | -1000     |         |
| master | -B               | SCHED\_OTHER     | 0                  | -15      | BE        | 0            | -999      |         |
| vip    | -C               | SCHED\_OTHER     | 0                  | -10      | BE        | 2            | -500      |         |
| spirit | -D               | SCHED\_OTHER     | 0                  |  10      | BE        | 4            |  500      |         |
| kiddie | -E               | SCHED\_OTHER     | 0                  |  15      | BE        | 6            |  666      |         |
| snail  | -F               | SCHED\_BATCH     | 0                  |  19      | BE        | 7            |  999      |         |
| litter | -G               | SCHED\_IDLE      | 0                  |  19      | IDLE      | 0            |  1000     | default |

Compiling
---------

Use `meson`.

Usage
-----

Typical usage:

`litter --class=snail -- some_weird_command --with arguments`

Distribution and Contribution
-----------------------------

Distributed under terms and conditions of GNU GPL v3 (only).

Developers:

* Oleksandr Natalenko &lt;oleksandr@natalenko.name&gt;
