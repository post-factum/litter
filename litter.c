/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab */

/*
 * litter — wrapper that runs subsequent command with various priorities
 * © Oleksandr Natalenko <oleksandr@natalenko.name>, 2017–2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <linux/oom.h>
#include <linux/sched.h>
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/syscall.h>
#include <sysexits.h>
#include <unistd.h>

enum
{
	IOPRIO_CLASS_RT = 1,
	IOPRIO_CLASS_BE,
	IOPRIO_CLASS_IDLE
};

#define IOPRIO_WHO_PROCESS             1
#define IOPRIO_CLASS_SHIFT            13
#define IOPRIO_BE_NR                   8
#define IOPRIO_PRIO_VALUE(class, data) (((class) << IOPRIO_CLASS_SHIFT) | (data))

#define OOM_SCORE_ADJ_FILE             "/proc/self/oom_score_adj"
#define OOM_SCORE_ADJ_MAX_BUF_LEN      6

#define PRIO_CLASS_DEITY               "deity"
#define PRIO_CLASS_MASTER              "master"
#define PRIO_CLASS_VIP                 "vip"
#define PRIO_CLASS_SPIRIT              "spirit"
#define PRIO_CLASS_KIDDIE              "kiddie"
#define PRIO_CLASS_SNAIL               "snail"
#define PRIO_CLASS_LITTER              "litter"
#define DEFAULT_PRIO_CLASS             PRIO_CLASS_LITTER

static struct prio_class
{
	const char* name;
	unsigned int sched_class;
	unsigned int sched_priority;
	int niceness;
	unsigned int iosched_class;
	unsigned int ioprio;
	int oom_score;
} prio_classes[] =
{
	{ PRIO_CLASS_DEITY,   SCHED_RR,    99, PRIO_MIN,         IOPRIO_CLASS_RT,                  0    , OOM_SCORE_ADJ_MIN         },
	{ PRIO_CLASS_MASTER,  SCHED_OTHER,  0, PRIO_MIN / 4 * 3, IOPRIO_CLASS_BE,                  0    , OOM_SCORE_ADJ_MIN + 1     },
	{ PRIO_CLASS_VIP,     SCHED_OTHER,  0, PRIO_MIN / 2,     IOPRIO_CLASS_BE,   IOPRIO_BE_NR / 4    , OOM_SCORE_ADJ_MIN / 2     },
	{ PRIO_CLASS_SPIRIT,  SCHED_OTHER,  0, PRIO_MAX / 2,     IOPRIO_CLASS_BE,   IOPRIO_BE_NR / 2    , OOM_SCORE_ADJ_MAX / 2     },
	{ PRIO_CLASS_KIDDIE,  SCHED_OTHER,  0, PRIO_MAX / 4 * 3, IOPRIO_CLASS_BE,   IOPRIO_BE_NR / 4 * 3, OOM_SCORE_ADJ_MAX / 3 * 2 },
	{ PRIO_CLASS_SNAIL,   SCHED_BATCH,  0, PRIO_MAX - 1,     IOPRIO_CLASS_BE,   IOPRIO_BE_NR - 1    , OOM_SCORE_ADJ_MAX - 1     },
	{ PRIO_CLASS_LITTER,  SCHED_IDLE,   0, PRIO_MAX - 1,     IOPRIO_CLASS_IDLE,                0    , OOM_SCORE_ADJ_MAX         },
	{ NULL, 0, 0, 0, 0, 0, 0 }
};

static int get_prio_class(const char* _name, struct prio_class** _prio_class)
{
	int ret = -1;
	struct prio_class* res = NULL;

	for (res = prio_classes; res->name; res++)
		if (!strncmp(_name, res->name, strlen(res->name)))
		{
			ret = 0;
			*_prio_class = res;
			break;
		}

	return ret;
}

int main(int _argc, char** _argv)
{
	int opts = 0;
	struct prio_class* prio_class = NULL;
	struct sched_param schd_prm;
	char* file = NULL;
	char** argv = NULL;
	char* argvshell[2] = { NULL, };

	memset(&schd_prm, 0, sizeof schd_prm);

	struct option longopts[] =
	{
		{ "A",     no_argument,       NULL, 'a' },
		{ "B",     no_argument,       NULL, 'b' },
		{ "C",     no_argument,       NULL, 'c' },
		{ "D",     no_argument,       NULL, 'd' },
		{ "E",     no_argument,       NULL, 'e' },
		{ "F",     no_argument,       NULL, 'f' },
		{ "G",     no_argument,       NULL, 'g' },
		{ "class", required_argument, NULL, 'z' },
		{ 0, 0, 0, 0 }
	};

	while ((opts = getopt_long_only(_argc, _argv, "abcdefgz", longopts, NULL)) != -1)
	{
		switch (opts)
		{
			case 'a':
				if (get_prio_class(PRIO_CLASS_DEITY, &prio_class) == -1)
					exit(EX_SOFTWARE);
				break;
			case 'b':
				if (get_prio_class(PRIO_CLASS_MASTER, &prio_class) == -1)
					exit(EX_SOFTWARE);
				break;
			case 'c':
				if (get_prio_class(PRIO_CLASS_VIP, &prio_class) == -1)
					exit(EX_SOFTWARE);
				break;
			case 'd':
				if (get_prio_class(PRIO_CLASS_SPIRIT, &prio_class) == -1)
					exit(EX_SOFTWARE);
				break;
			case 'e':
				if (get_prio_class(PRIO_CLASS_KIDDIE, &prio_class) == -1)
					exit(EX_SOFTWARE);
				break;
			case 'f':
				if (get_prio_class(PRIO_CLASS_SNAIL, &prio_class) == -1)
					exit(EX_SOFTWARE);
				break;
			case 'g':
				if (get_prio_class(PRIO_CLASS_LITTER, &prio_class) == -1)
					exit(EX_SOFTWARE);
				break;
			case 'z':
				if (get_prio_class(optarg, &prio_class) == -1)
					exit(EX_USAGE);
				break;
			default:
				exit(EX_USAGE);
				break;
		}
	}

	// if no command is provided, start $SHELL
	if (optind >= _argc)
	{
		file = getenv("SHELL");
		if (!file)
			exit(EX_USAGE);
		argvshell[0] = file;
		argv = argvshell;
	} else
	{
		file = _argv[optind];
		argv = &_argv[optind];
	}

	if (!prio_class && get_prio_class(DEFAULT_PRIO_CLASS, &prio_class) == -1)
		exit(EX_SOFTWARE);

	// niceness
	if (setpriority(PRIO_PROCESS, 0, prio_class->niceness) == -1)
	{
		perror("setpriority");
		exit(EX_OSERR);
	}

	// sched class and priority
	schd_prm.sched_priority = prio_class->sched_priority;
	if (sched_setscheduler(0, prio_class->sched_class, &schd_prm) == -1)
	{
		perror("sched_setscheduler");
		exit(EX_OSERR);
	}

	// I/O class and priority
	if (syscall(SYS_ioprio_set, IOPRIO_WHO_PROCESS, 0, IOPRIO_PRIO_VALUE(prio_class->iosched_class, prio_class->ioprio)) == -1)
	{
		perror("ioprio_set");
		exit(EX_OSERR);
	}

	// OOM score
	int fd = open(OOM_SCORE_ADJ_FILE, O_WRONLY);
	if (fd == -1)
	{
		perror("open");
		exit(EX_IOERR);
	}
	char buf[OOM_SCORE_ADJ_MAX_BUF_LEN];
	int buflen = snprintf(buf, sizeof(buf), "%d", prio_class->oom_score);
	if (buflen < 0)
		exit(EX_SOFTWARE);
	if (write(fd, buf, buflen + 1) == -1)
	{
		perror("write");
		if (errno == EACCES)
			exit(EX_NOPERM);
		else
			exit(EX_IOERR);
	}
	if (close(fd) == -1)
	{
		perror("close");
		exit(EX_IOERR);
	}

	// go!
	if (execvp(file, argv) == -1)
	{
		perror("execvp");
		exit(EX_SOFTWARE);
	}
}

